package am.iunetworks.bpmsuite.engine;

import am.iunetworks.bpmsuite.engine.helper.KafkaAssertions;
import am.iunetworks.bpmsuite.engine.helper.KafkaEventCollector;
import am.iunetworks.bpmsuite.engine.helper.ProcessAssertions;
import am.iunetworks.bpmsuite.engine.helper.ProcessLogCollector;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.flowable.common.engine.api.delegate.event.FlowableEngineEventType;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.impl.event.logger.handler.Fields;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.variable.api.history.HistoricVariableInstance;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@EmbeddedKafka(topics = { "send-greeting.request", "send-greeting.response" })
public class ServiceProxyTaskHandlerIT {
    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private EmbeddedKafkaBroker kafkaBroker;

    @Autowired
    private KafkaTemplate<String, Map<String, Object>> kafkaTemplate;

    private ProcessLogCollector processLogCollector;
    private ProcessAssertions processAssertions;

    private KafkaEventCollector kafkaEventCollector;
    private KafkaAssertions kafkaAssertions;

    @BeforeEach
    void setUp() {
        processLogCollector = new ProcessLogCollector();
        processAssertions = new ProcessAssertions(processLogCollector);
        runtimeService.addEventListener(processLogCollector);

        kafkaEventCollector = new KafkaEventCollector();
        kafkaAssertions = new KafkaAssertions(kafkaEventCollector);
    }

    @AfterEach
    void tearDown() {
        runtimeService.removeEventListener(processLogCollector);
    }

    @Test
    public void testServiceTaskStarted() throws InterruptedException {
        String recipient = "Flowable";
        String processId = startGreetingTaskWithRecipient(recipient);

        Thread.sleep(1000);

        processAssertions.assertActivityStarted("send-greeting");
        processAssertions.assertActivityNotCompleted("send-greeting");

        Map<String, Object> filter = new HashMap<>();
        filter.put("recipient", recipient);
        kafkaAssertions.assertEventReceived("send-greeting.request", filter);
    }

    @Test
    public void testServiceTaskCompleted() throws InterruptedException {
        String recipient = "Flowable";
        String processId = startGreetingTaskWithRecipient(recipient);

        Thread.sleep(1000);

        String greeting = String.format("Hello, %s!", recipient);
        List<Map<String, Object>> activityStartedEvents = processLogCollector.findEvents(FlowableEngineEventType.ACTIVITY_STARTED, "send-greeting");
        String executionId = null;
        if (!activityStartedEvents.isEmpty()) {
            executionId = (String) activityStartedEvents.get(0).get(Fields.EXECUTION_ID);
        }
        Map<String, Object> payload = new HashMap<>();
        payload.put("greeting", greeting);
        payload.put("execution-id", executionId);
        kafkaTemplate.send("send-greeting.response", UUID.randomUUID().toString(), payload);

        Thread.sleep(1000);

        processAssertions.assertActivityStarted("send-greeting");
        processAssertions.assertActivityCompleted("send-greeting");

        Map<String, Object> expectedOutputData = new HashMap<>();
        expectedOutputData.put("greeting", greeting);
        List<HistoricVariableInstance> variables = historyService.createHistoricVariableInstanceQuery().processInstanceId(processId).list();
        variables = variables.stream().filter(variable -> "taskOutput".equals(variable.getVariableName())).collect(Collectors.toList());
        assertThat(variables, not(empty()));
        HistoricVariableInstance ouputVariable = variables.get(0);
        assertEquals(expectedOutputData, ouputVariable.getValue());
    }

    private String startGreetingTaskWithRecipient(String recipient) {
        Map<String, Object> variables = new HashMap<>();
        variables.put("recipient", recipient);
        ProcessInstance instance = runtimeService.startProcessInstanceByKey("greeting", variables);
        return instance.getId();
    }

    @KafkaListener(groupId = "test", topicPattern = ".*\\.request")
    private void eventReceived(ConsumerRecord<String, Map<String, Object>> event) {
        kafkaEventCollector.eventReceived(event);
    }
}

package am.iunetworks.bpmsuite.engine.helper;

import org.flowable.common.engine.api.delegate.event.FlowableEngineEvent;
import org.flowable.common.engine.api.delegate.event.FlowableEntityEvent;
import org.flowable.common.engine.api.delegate.event.FlowableEventType;
import org.flowable.common.engine.impl.interceptor.CommandContext;
import org.flowable.engine.delegate.event.FlowableEntityWithVariablesEvent;
import org.flowable.engine.impl.event.logger.handler.AbstractDatabaseEventLoggerEventHandler;
import org.flowable.engine.impl.persistence.entity.EventLogEntryEntity;

import java.util.HashMap;
import java.util.Map;

public class DefaultEventHandler extends AbstractDatabaseEventLoggerEventHandler {
    @Override
    public EventLogEntryEntity generateEventLogEntry(CommandContext commandContext) {
        FlowableEventType type = this.event.getType();
        String processDefinitionId = null;
        String processInstanceId = null;
        String executionId = null;
        String taskId = null;
        Map<String, Object> data = new HashMap<>();

        if (this.event instanceof FlowableEntityEvent) {
            FlowableEntityEvent entityEvent = (FlowableEntityEvent) this.event;
            this.putInMapIfNotNull(data, "entity", entityEvent.getEntity().toString());
        }
        if (this.event instanceof FlowableEntityWithVariablesEvent) {
            FlowableEntityWithVariablesEvent entityWithVariablesEvent = (FlowableEntityWithVariablesEvent) this.event;
            this.putInMapIfNotNull(data, "variables", entityWithVariablesEvent.getVariables());
            this.putInMapIfNotNull(data, "isLocalScope", entityWithVariablesEvent.isLocalScope());
        }
        if (this.event instanceof FlowableEngineEvent) {
            FlowableEngineEvent engineEvent = (FlowableEngineEvent) this.event;
            processDefinitionId = engineEvent.getProcessDefinitionId();
            processInstanceId = engineEvent.getProcessInstanceId();
            executionId = engineEvent.getExecutionId();
        }
        return this.createEventLogEntry(type.name(), processDefinitionId, processInstanceId, executionId, taskId, data);
    }
}

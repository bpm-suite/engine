package am.iunetworks.bpmsuite.engine.helper;

import org.flowable.common.engine.api.delegate.event.FlowableEngineEventType;
import org.flowable.engine.impl.event.logger.handler.Fields;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.*;

public class ProcessAssertions {
    private final ProcessLogCollector processLogCollector;

    public ProcessAssertions(ProcessLogCollector processLogCollector) {
        this.processLogCollector = processLogCollector;
    }

    public void assertActivityStarted(String activityId) {
        List<Map<String, Object>> filteredEvents = filterEvents(FlowableEngineEventType.ACTIVITY_STARTED, activityId);
        assertThat(String.format("Activity %s has not been started", activityId), filteredEvents.size() == 1);
    }

    public void assertActivityNotStarted(String activityId) {
        List<Map<String, Object>> filteredEvents = filterEvents(FlowableEngineEventType.ACTIVITY_STARTED, activityId);
        assertThat(String.format("Activity %s has been started", activityId), filteredEvents.size() == 0);
    }

    public void assertActivityCompleted(String activityId) {
        List<Map<String, Object>> filteredEvents = filterEvents(FlowableEngineEventType.ACTIVITY_COMPLETED, activityId);
        assertThat(String.format("Activity %s has not been completed", activityId), filteredEvents.size() == 1);
    }

    public void assertActivityNotCompleted(String activityId) {
        List<Map<String, Object>> filteredEvents = filterEvents(FlowableEngineEventType.ACTIVITY_COMPLETED, activityId);
        assertThat(String.format("Activity %s has been completed", activityId), filteredEvents.size() == 0);
    }

    private List<Map<String, Object>> filterEvents(FlowableEngineEventType eventType, String activityId) {
        return processLogCollector.events.stream().filter(event ->
                (eventType.toString() == event.get(ProcessLogCollector.FIELD_TYPE)) && (activityId.equals(event.get(Fields.ACTIVITY_ID)))
        ).collect(Collectors.toList());
    }
}

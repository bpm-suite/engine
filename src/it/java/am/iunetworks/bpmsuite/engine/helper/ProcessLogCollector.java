package am.iunetworks.bpmsuite.engine.helper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.flowable.common.engine.api.delegate.event.FlowableEngineEventType;
import org.flowable.common.engine.api.delegate.event.FlowableEvent;
import org.flowable.common.engine.impl.interceptor.CommandContext;
import org.flowable.common.engine.impl.util.DefaultClockImpl;
import org.flowable.engine.delegate.event.FlowableProcessStartedEvent;
import org.flowable.engine.impl.event.logger.AbstractEventFlusher;
import org.flowable.engine.impl.event.logger.EventFlusher;
import org.flowable.engine.impl.event.logger.EventLogger;
import org.flowable.engine.impl.event.logger.handler.EventLoggerEventHandler;
import org.flowable.engine.impl.event.logger.handler.Fields;
import org.flowable.engine.impl.persistence.entity.EventLogEntryEntity;
import org.flowable.engine.impl.persistence.entity.ExecutionEntity;
import org.flowable.engine.runtime.ProcessInstance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProcessLogCollector extends EventLogger {
    public static final String FIELD_TYPE = "FIELD_TYPE";

    public final List<Map<String, Object>> events = new ArrayList<>();

    private ProcessInstance processInstance;

    public ProcessLogCollector() {
        super(new DefaultClockImpl(), new ObjectMapper());
    }

    @Override
    protected void initializeDefaultHandlers() {
        super.initializeDefaultHandlers();
        for (FlowableEngineEventType eventType: FlowableEngineEventType.values()) {
            if (!eventHandlers.containsKey(eventType)) {
                addEventHandler(eventType, DefaultEventHandler.class);
            }
        }
    }

    @Override
    protected EventFlusher createEventFlusher() {
        return new AbstractEventFlusher() {
            @Override
            public void closing(CommandContext commandContext) {
            }

            @Override
            public void afterSessionsFlush(CommandContext commandContext) {
                flush(commandContext);
            }

            @Override
            public void closeFailure(CommandContext commandContext) {
            }

            private void flush(CommandContext commandContext) {
                for (EventLoggerEventHandler eventHandler: eventHandlers) {
                    EventLogEntryEntity eventLogEntryEntity = eventHandler.generateEventLogEntry(commandContext);
                    try {
                        TypeReference<HashMap<String, Object>> typeRef = new TypeReference<>() {};
                        Map<String, Object> map = objectMapper.readValue(eventLogEntryEntity.getData(), typeRef);
                        map.put(FIELD_TYPE, eventLogEntryEntity.getType());
                        events.add(map);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    @Override
    public void onEvent(FlowableEvent event) {
        if (event instanceof FlowableProcessStartedEvent) {
            FlowableProcessStartedEvent processStartedEvent = (FlowableProcessStartedEvent) event;
            if (processStartedEvent.getEntity() instanceof ExecutionEntity) {
                ExecutionEntity executionEntity = (ExecutionEntity) processStartedEvent.getEntity();
                processInstance = executionEntity.getProcessInstance();
            }
        }
        try {
            super.onEvent(event);
        } catch (NullPointerException ignored) {}
    }

    public List<Map<String, Object>> findEvents(FlowableEngineEventType eventType, String activityId) {
         return events.stream().filter(event ->
                (eventType.toString() == event.get(ProcessLogCollector.FIELD_TYPE)) && (activityId.equals(event.get(Fields.ACTIVITY_ID)))
         ).collect(Collectors.toList());
    }
}

package am.iunetworks.bpmsuite.engine.helper;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class KafkaEventCollector {
    public static final String TOPIC_NAME = "TOPIC_NAME";

    public List<Map<String, Object>> events = new ArrayList<>();

    public void eventReceived(ConsumerRecord<String, Map<String, Object>> event) {
        String topic = event.topic();
        Map<String, Object> payload = event.value();
        payload.put(TOPIC_NAME, topic);
        events.add(payload);
    }
}

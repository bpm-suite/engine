package am.iunetworks.bpmsuite.engine.helper;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;

public class KafkaAssertions {
    private KafkaEventCollector eventCollector;

    public KafkaAssertions(KafkaEventCollector eventCollector) {
        this.eventCollector = eventCollector;
    }

    public void assertEventReceived(String topic, Map<String, Object> filter) {
        List<Map<String, Object>> filteredEvents = filterEvents(topic, filter);
        assertThat(String.format("Event on topic %s has not been received", topic), filteredEvents.size() == 1);
    }

    private List<Map<String, Object>> filterEvents(String topic, Map<String, Object> filter) {
        return eventCollector.events.stream().filter(event -> {
            boolean result = topic.equals(event.get(KafkaEventCollector.TOPIC_NAME));
            for (String key: filter.keySet()) {
                result &= filter.get(key).equals(event.get(key));
            }
            return result;
        }).collect(Collectors.toList());
    }
}

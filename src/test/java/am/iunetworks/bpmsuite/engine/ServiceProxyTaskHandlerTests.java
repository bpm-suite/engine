package am.iunetworks.bpmsuite.engine;

import am.iunetworks.bpmsuite.engine.handlers.ServiceProxyTaskHandler;
import am.iunetworks.bpmsuite.engine.messaging.Message;
import am.iunetworks.bpmsuite.engine.messaging.MessageListener;
import am.iunetworks.bpmsuite.engine.messaging.MessagingClient;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = { BeanUtil.class })
@ExtendWith(MockitoExtension.class)
public class ServiceProxyTaskHandlerTests {
    private static final String EXECUTION_ID_KEY = "execution-id";
    private static final String COMMAND_KEY = "command";
    private static final String PAYLOAD_KEY = "payload";
    private static final String RECIPIENT_KEY = "recipient";
    private static final String GREETING_KEY = "greeting";

    private static final String TEST_EXECUTION_ID = "test-id";
    private static final String TEST_COMMAND = "send-greeting";
    private static final String TEST_RECIPIENT = "Flowable";

    private final ServiceProxyTaskHandler handler = new ServiceProxyTaskHandler();

    @Mock
    private DelegateExecution executionMock;

    @MockBean
    private RuntimeService runtimeServiceMock;

    @MockBean
    private MessagingClient messagingClientMock;

    @Test
    public void testExecution() {
        Map<String, Object> data = createInputData();
        when(executionMock.getVariable("taskInput")).thenReturn(data);
        when(executionMock.getId()).thenReturn(TEST_EXECUTION_ID);

        handler.execute(executionMock);

        Message expectedMessage = createRequestMessage();
        verify(messagingClientMock).sendMessage(expectedMessage);
    }

    @Test
    public void testCompletion() {
        Map<String, Object> data = createInputData();
        when(executionMock.getVariable("taskInput")).thenReturn(data);
        when(executionMock.getId()).thenReturn(TEST_EXECUTION_ID);

        handler.execute(executionMock);

        ArgumentCaptor<MessageListener> messageListenerCaptor = ArgumentCaptor.forClass(MessageListener.class);
        verify(messagingClientMock).addListener(messageListenerCaptor.capture());

        MessageListener listener = messageListenerCaptor.getValue();

        String expectedTopic = String.format("%s.response", data.get("command"));
        Map<String, Object> expectedFilter = new HashMap<>();
        expectedFilter.put(EXECUTION_ID_KEY, TEST_EXECUTION_ID);
        assertEquals(expectedTopic, listener.getTopic());
        assertEquals(expectedFilter, listener.getFilter());

        Message responseMessage = createResponseMessage();
        listener.messageReceived(responseMessage);

        Map<String, Object> expectedOutputData = createOutputData();
        verify(runtimeServiceMock).triggerAsync(TEST_EXECUTION_ID, expectedOutputData);
    }

    private Map<String, Object> createInputData() {
        Map<String, Object> payload = new HashMap<>();
        payload.put(RECIPIENT_KEY, TEST_RECIPIENT);
        Map<String, Object> data = new HashMap<>();
        data.put(COMMAND_KEY, TEST_COMMAND);
        data.put(PAYLOAD_KEY, payload);
        return data;
    }

    private Map<String, Object> createOutputData() {
        Map<String, Object> data = new HashMap<>();
        data.put(GREETING_KEY, String.format("Hello, %s!", TEST_RECIPIENT));
        Map<String, Object> variables = new HashMap<>();
        variables.put("taskOutput", data);
        return variables;
    }

    private Message createRequestMessage() {
        String topic = String.format("%s.request", TEST_COMMAND);
        Map<String, Object> payload = new HashMap<>();
        payload.put(EXECUTION_ID_KEY, TEST_EXECUTION_ID);
        payload.put(RECIPIENT_KEY, TEST_RECIPIENT);
        return new Message(topic, payload);
    }

    private Message createResponseMessage() {
        String topic = String.format("%s.response", TEST_COMMAND);
        Map<String, Object> payload = new HashMap<>();
        payload.put(EXECUTION_ID_KEY, TEST_EXECUTION_ID);
        payload.put(GREETING_KEY, String.format("Hello, %s!", TEST_RECIPIENT));
        return new Message(topic, payload);
    }
}

package am.iunetworks.bpmsuite.engine.handlers;

import am.iunetworks.bpmsuite.engine.BeanUtil;
import am.iunetworks.bpmsuite.engine.messaging.AbstractMessageListener;
import am.iunetworks.bpmsuite.engine.messaging.Message;
import am.iunetworks.bpmsuite.engine.messaging.MessagingClient;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.impl.delegate.TriggerableActivityBehavior;

import java.util.HashMap;
import java.util.Map;

public class ServiceProxyTaskHandler implements TriggerableActivityBehavior {
    private static final String EXECUTION_ID_KEY = "execution-id";
    private static final String TASK_INPUT_KEY = "taskInput";
    private static final String TASK_OUTPUT_KEY = "taskOutput";

    @Override
    public void execute(DelegateExecution delegateExecution) {
        String executionId = delegateExecution.getId();
        Map<String, Object> data = (Map<String, Object>) delegateExecution.getVariable(TASK_INPUT_KEY);

        MessagingClient messagingClient = BeanUtil.getBean(MessagingClient.class);

        String responseTopic = String.format("%s.response", data.get("command"));
        Map<String, Object> filter = new HashMap<>();
        filter.put(EXECUTION_ID_KEY, executionId);
        messagingClient.addListener(new AbstractMessageListener(responseTopic, filter) {
            @Override
            public void messageReceived(Message message) {
                messagingClient.removeListener(this);
                RuntimeService runtimeService = BeanUtil.getBean(RuntimeService.class);
                Map<String, Object> outputData = message.getPayload();
                outputData.remove(EXECUTION_ID_KEY);
                Map<String, Object> outputVariables = new HashMap<>();
                outputVariables.put(TASK_OUTPUT_KEY, outputData);
                runtimeService.triggerAsync(executionId, outputVariables);
            }
        });
        messagingClient.sendMessage(assembleMessage(data, executionId));
    }

    private Message assembleMessage(Map<String, Object> data, String executionId) {
        String topic = String.format("%s.request", data.get("command"));
        Map<String, Object> payload = (Map<String, Object>) data.get("payload");
        payload.put(EXECUTION_ID_KEY, executionId);
        return new Message(topic, payload);
    }

    @Override
    public void trigger(DelegateExecution delegateExecution, String s, Object o) {
    }
}

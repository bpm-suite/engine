package am.iunetworks.bpmsuite.engine.messaging;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class Message {
    private String topic;
    private String key;
    private Map<String, Object> payload;

    public Message(String topic) {
        this(topic, null);
    }

    public Message(String topic, Map<String, Object> payload) {
        this(topic, UUID.randomUUID().toString(), payload);
    }

    public Message(String topic, String key, Map<String, Object> payload) {
        this.key = key;
        this.topic = topic;
        this.payload = payload;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Map<String, Object> getPayload() {
        return payload;
    }

    public void setPayload(Map<String, Object> payload) {
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return topic.equals(message.topic) &&
                Objects.equals(payload, message.payload);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topic, payload);
    }
}

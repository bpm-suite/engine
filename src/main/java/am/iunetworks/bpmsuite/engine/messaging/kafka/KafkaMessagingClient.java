package am.iunetworks.bpmsuite.engine.messaging.kafka;

import am.iunetworks.bpmsuite.engine.messaging.Message;
import am.iunetworks.bpmsuite.engine.messaging.MessageListener;
import am.iunetworks.bpmsuite.engine.messaging.MessagingClient;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class KafkaMessagingClient implements MessagingClient {
    private Set<MessageListener> listeners = new HashSet<>();

    @Autowired
    private KafkaTemplate<String, Map<String, Object>> kafkaTemplate;

    @Override
    public void sendMessage(Message message) {
        try {
            System.out.println(String.format("***** Sending message: Topic: %s, Key: %s, Payload: %s", message.getTopic(), message.getKey(), message.getPayload()));
            kafkaTemplate.send(message.getTopic(), message.getKey(), message.getPayload()).get();
            System.out.println(String.format("***** Message sent successfully: Topic: %s, Key: %s, Payload: %s", message.getTopic(), message.getKey(), message.getPayload()));
        } catch (InterruptedException e) {
            System.out.println(String.format("***** Interrupted exception: Topic: %s, Key: %s, Payload: %s", message.getTopic(), message.getKey(), message.getPayload()));
        } catch (ExecutionException e) {
            System.out.println(String.format("***** Execution exception: Topic: %s, Key: %s, Payload: %s", message.getTopic(), message.getKey(), message.getPayload()));
        }

    }

    @KafkaListener(topicPattern = ".*")
    private void eventReceived(ConsumerRecord<String, Map<String, Object>> event) {
        notifyListeners(event);
    }

    @Override
    public void addListener(MessageListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(MessageListener listener) {
        listeners.remove(listener);
    }

    @Override
    public Set<MessageListener> getListeners() {
        return listeners;
    }

    private void notifyListeners(ConsumerRecord<String, Map<String, Object>> event) {
        String topic = event.topic();
        String key = event.key();
        Map<String, Object> payload = event.value();
        Message message = new Message(event.topic(), key, payload);

        List<MessageListener> topicListeners = listeners.stream().filter(listener -> topic.equals(listener.getTopic())).collect(Collectors.toList());
        for (MessageListener listener: topicListeners) {
            Map<String, Object> filter = listener.getFilter();
            boolean notify = true;
            for (String filterKey: filter.keySet()) {
                if (!filter.get(filterKey).equals(payload.get(filterKey))) {
                    notify = false;
                    break;
                }
            }
            if (notify) {
                listener.messageReceived(message);
            }
        }

    }
}
